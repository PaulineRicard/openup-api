# Open-up-api

### DESCRIPTION DU PROJET
Développement d'une application basé sur un système de livraison collaboratif, 
les livraisons se faisant entre particuliers inscrits à une application mobile, 
ainsi qu'à des commerçants partenaires, inscrits également à l'appli mobile afin 
de proposer une boutique et une liste de produits à commander.

Cette application est divisée en trois parties:
- Application mobile
- Interface d'administration
- Api 

La partie qui nous concerne ici est l'API.

### Outils de développement
- Framework PHP Symfony 4
- Api platform
- Base de données MySql
- Git et Gitlab pour le versionning 


### Installation
Lien du dépot: [https://gitlab.com/beweb-beziers-03/open-up-api]  
Dans un terminal
1. A la racine du serveur:  
      - $ mkdir openup  
      - $ cd openup

2. Récupération du dépot   
      - $ git init  
      - $ git remote add origin git@gitlab.com:beweb-beziers-03/open-up-api.git      
      - $ git clone https://gitlab.com/beweb-beziers-03/open-up-api.git
      - $ git pull origin dev-api

4. Création de la BDD  
      - Dans MySql créer une nouvelle base de données: openup  
      - Dans le fichier .env, changer la ligne suivante selon votre config:  
      DATABASE_URL=mysql://phpmyadmin:root@127.0.0.1:3306/openup
           
3. Configuration  
      - $ composer install  
      - $ bin/console doctrine:database:create
      - $ bin/console doctrine:migrations:migrate  
      - $ bin/console doctrine:fixtures:load --append  
      
    ENJOY !!!     
  

