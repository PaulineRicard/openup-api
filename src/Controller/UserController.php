<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SponsorController constructor.
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     */
    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    /**
     * Méthode appelé lors de la creation d'un compte user
     * @Route("/user", name="user")
     * @param $account
     * @param $dataTab
     * @return User
     */
   public function create($account, $dataTab)
    {
        $coordinate = $this->getCoordinate($dataTab['address']['address'], $dataTab['address']['zipcode']);
        $user = new User();
        $user->setBalanceWallet(0)
            ->setXCoordinate($coordinate[0])
            ->setYCoordinate($coordinate[1])
            ->setAccount($account);
        // On enregistre l'utilisateur dans la base
        $this->manager->persist($user);
        return $user;
    }

    /**
     * Recupere les coordonnées gps grace a l'API adresse.data.fr
     * @param $address
     * @param $zipcode
     * @return resource|string
     */
    public function getCoordinate($address, $zipcode)
    {
        $url = "https://api-adresse.data.gouv.fr/search/?q=" . str_replace(' ', '+', $address) . "&postcode=" . $zipcode;
        $raw = file_get_contents($url);
        $json = $this->serializer->decode($raw, 'json');
        return $json['features'][0]['geometry']['coordinates'];
    }

    /**
     * Recupere l'account par l'id du user. Requete sql dans le UserRepository
     * @Route("/api/users/{id}/account")
     * @param $id
     * @return object[]
     */
    public function getUserAcc($id)
    {
        $users = $this->getDoctrine()->getRepository(User::class)->getUserAccount($id);
        return new JsonResponse($users);
    }
}
