<?php

namespace App\Controller;

use App\Entity\Sponsor;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class SponsorController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SponsorController constructor.
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     */
    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    /**
     * Create sponsor
     * @param $account
     * @param $dataTab
     * @return Sponsor|Response
     */
    public function create($account, $dataTab)
    {
        $sponsor = new Sponsor();
        $sponsor->setName($dataTab['name'])
                ->setAccount($account);
        $this->manager->persist($sponsor);
        return $sponsor;
    }

    /**
     * Modifie un sponsor par son id passé en paramètres
     * @param $id
     * @return JsonResponse
     */
    public function update($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $sponsor = $entityManager->getRepository(Sponsor::class)->find($id);

        if(!$sponsor) {
           throw $this->createNotFoundException(
               'Sponsor non trouvé'
           );
       }

        $sponsor->setName('name');

        $entityManager->flush();

        return new JsonResponse("Modification ok", Response::HTTP_CREATED, [], true);
    }

    /**
     * Retourne un sponsor par son id passé en paramètre
     * @param $id
     */
    public function get($id)
    {
        $sponsors = $this->getDoctrine()
            ->getRepository(Gratification::class)
            ->find($id);
        if(!$sponsors) {
            throw $this->createNotFoundException(
                'Sponsor non trouvé'
            );
        }
    }

    /**
     * Supprime un sponsor
     * @param $id
     */
    public function deleteSponsor($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $sponsor = $entityManager->getRepository(Sponsor::class)->find($id);

        if(!$sponsor) {
            throw $this->createNotFoundException('Sponsor non trouvé');
        }
        $entityManager->remove($sponsor);
        $entityManager->flush();
    }
}
