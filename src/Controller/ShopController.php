<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Shop;
use App\Entity\ShopType;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;


class ShopController extends AbstractController
{
    private $manager;
    private $serializer;

    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }


    /**
     * Crée un shop
     * @param $trader
     * @param $dataTab
     * @param $type
     * @return Shop
     */
    public function create($trader, $dataTab, $type)
    {
        $coordinate = $this->getCoordinate($dataTab['address']['address'], $dataTab['address']['zipcode']);
        $shop = new Shop();
        $shop->setName($dataTab['shop']['name'])
            ->setXCoordinate($coordinate[0])
            ->setYCoordinate($coordinate[1])
            ->setShopType($type)
            ->setTrader($trader);

        $this->manager->persist($shop);
        return $shop;
    }

    /**
     * Retourne une boutique par son id passé en paramètre
     * @Route("/api/shops/{id}/items", methods={"GET"})
     * @param $id
     * @return object[]
     */
    public function get($id)
    {
        $items = $this->getDoctrine()->getRepository('App:Item')->findAll();
        //dd($items);
        $shop = $this->getDoctrine()
            ->getRepository(Shop::class)
            ->find($id);

        if(!$shop) {
            throw $this->createNotFoundException(
                'Boutique non trouvée'
            );
        }
        return $items;
    }

    /**
     * Retourne la liste des boutiques
     *
     */
    public function getShops()
    {
        $shop = $this->getDoctrine()->getRepository(Shop::class)->findAll();

        if(!$shop) {
            throw $this->createNotFoundException('Erreur, liste non trouvée');
        }
        return $shop->fetchAll();
    }

//    /**
//     * Modification d'une boutique
//     * @Route("/api/shops/{id}, name="shop_show")
//     * @param $id
//     * @return \Symfony\Component\HttpFoundation\RedirectResponse
//     */
//    public function updateShop($id)
//    {
//        $entityManager = $this->getDoctrine()->getManager();
//        $shop = $entityManager->getRepository(Shop::class)->find($id);
//
//        if(!$shop) {
//            throw $this->createNotFoundException(
//                'Boutique non trouvée'
//            );
//        }
//
//        $shop->setName('name')
//        ->setXCoordinate('X_coordinate')
//        ->setYCoordinate('Y_coordinate');
//        $entityManager->flush();
//
//        return $this->redirectToRoute();
//    }

    /**
     * Supprime une boutique
     * @param $id
     */
    public function deleteShop($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $shop = $entityManager->getRepository(Shop::class)->find($id);

        if(!$shop) {
            throw $this->createNotFoundException(
                'Gratification non trouvée'
            );
        }

        $entityManager->remove($shop);
        $entityManager->flush();
    }

    /**
     * Recupere les coordonées grace a l'addresse
     * @param $address
     * @param $zipcode
     * @return resource|string
     */
    public function getCoordinate($address, $zipcode)
    {
        $url = "https://api-adresse.data.gouv.fr/search/?q=" . str_replace(' ', '+', $address) . "&postcode=" . $zipcode;
        $raw = file_get_contents($url);
        $json = $this->serializer->decode($raw, 'json');
        return $json['features'][0]['geometry']['coordinates'];
    }
}
