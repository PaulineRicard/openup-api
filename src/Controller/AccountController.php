<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Address;
use App\Entity\EmailSend;
use App\Events;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class AccountController extends AbstractController
{

    private $passwordEncoder;
    private $serializer;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SerializerInterface $serializer)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->serializer = $serializer;
    }


    /**
     * Crée les compte users / trader / sponsor.
     * @Route("/api/account", name="account", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @param EventDispatcherInterface $eventDispatcher
     * @return JsonResponse
     */
    public function create(Request $request, SerializerInterface $serializer, ObjectManager $manager, EventDispatcherInterface $eventDispatcher)
    {
        try {
            // Récupere le contenu du formulaire.
            $data = $request->getContent();
            $dataTab = $serializer->decode($data, 'json');
            // Rempli la table adress
            $address = new Address();
            $address->setAddress($dataTab['address']['address']);
            $address->setCity($dataTab['address']['city']);
            $address->setZipcode($dataTab['address']['zipcode']);
            $manager->persist($address);
            $manager->flush();
            // On récupère l'entité Adresse qu'on peut inscrire avec le reste des données dans la table Account
            $account = new Account();
            $account->setFirstname($dataTab['firstname']);
            $account->setLastname($dataTab['lastname']);
            $account->setEmail($dataTab['email']);
            $account->setRole($dataTab['role']);
            $account->setStatus('waiting');
            $account->setPassword($this->passwordEncoder->encodePassword($account, $dataTab['password']));
            $account->setPhone($dataTab['phone']);
            $account->setAddress($address);
            $manager->persist($account);
            $manager->flush();
            // en fonction du role
            if ($dataTab['role'] === "user") {
                // inscription de la table user par UserController
                $user = new UserController($serializer, $manager);
                $user->create($account, $dataTab);
                // envoie de l'email de creation de compte par EmailController
                // $register_main = new EmailController($manager, $eventDispatcher);
                // $register_main->user_register_waiting($account);
            }
            if ($dataTab['role'] === "trader") {
                // Recuperation de la liste des types de commerce
                $type = $manager->getRepository('App:ShopType')->find($dataTab['shop']['shopType']);
                // inscription de la table trader par TraderController
                $trader = new TraderController($serializer, $manager);
                $trader->create($account, $dataTab, $type);
            }
            if ($dataTab['role'] === "sponsor") {
                // inscription de la sponsor trader par SponsorController
                $sponsor = new SponsorController($serializer, $manager);
                $sponsor->create($account, $dataTab);
            }
            $manager->flush();

            return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage(), Response::HTTP_BAD_REQUEST, [], true);
        }
    }

    /**
     * @Route("/api/account/{id}/validate", name="account_validate", methods={"POST"})
     * @param $id
     * @param ObjectManager $manager
     * @param EventDispatcherInterface $eventDispatcher
     * @return JsonResponse
     */
    public function validateAccount($id, ObjectManager $manager, EventDispatcherInterface $eventDispatcher)
    {
        $account = $manager->getRepository('App:Account')->find($id);
        if ($account && $account->getStatus() == 'waiting') {
            // Update la table Status
            $account->setStatus('validate');
            $manager->persist($account);
            $manager->flush();
            // envoie de l'email de creation de compte par EmailController
            // $register_main = new EmailController($manager, $eventDispatcher);
            // $register_main->user_register_validate($account);
            return new JsonResponse('{"message":"Account Validate","status":"201"}', Response::HTTP_CREATED, [], true);
        } else {
            return new JsonResponse('{"message":"Invalid Account","status":"400"}', Response::HTTP_BAD_REQUEST, [], true);
        }
    }

    /**
     * @Route("/api/account/{id}/blocked", name="account_blocked", methods={"POST"})
     * @param $id
     * @param ObjectManager $manager
     * @param EventDispatcherInterface $eventDispatcher
     * @return JsonResponse
     * @throws \Exception
     */
    public function blockedAccount($id, ObjectManager $manager, EventDispatcherInterface $eventDispatcher)
    {
        $account = $manager->getRepository('App:Account')->find($id);
        if ($account) {
            // Update la table Status
            $account->setStatus('blocked');
            $manager->persist($account);
            $manager->flush();
            // TODO envoie de l'email de blocage de compte par EmailController
            return new JsonResponse('{"message":"account blocked","status":"201"}', Response::HTTP_CREATED, [], true);
        } else {
            return new JsonResponse('{"message":"Invalid Account","status":"400"}', Response::HTTP_BAD_REQUEST, [], true);
        }
    }

    /**
     * Recupere l'account par l'email
     * @Route("/api/account/email/{email}", name="get_account_by_email", methods={"POST"})
     * @param $email
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function getAccountByEmail($email, ObjectManager $manager)
    {
        $account = $this->getDoctrine()->getRepository('App:Account')->findByEmail($email);
        $accountId = $account[0]->getId();
        $accountRole = $account[0]->getRole();
        if ($accountRole == "user") {
            $user = $manager->getRepository('App:User')->findBy(["account" => $accountId]);
            $user = $this->get('serializer')->serialize($user, 'json');
            return new JsonResponse($user, Response::HTTP_OK, [], true);
        } else if ($accountRole == "trader") {
            $trader = $manager->getRepository('App:Trader')->findBy(["account" => $accountId]);
            $trader = $this->get('serializer')->serialize($trader, 'json');
            return new JsonResponse($trader, Response::HTTP_OK, [], true);
        } else {
            return new JsonResponse("Bad Request", Response::HTTP_BAD_REQUEST, [], true);
        }
    }

}

