<?php

namespace App\Controller;

use App\Entity\EmailSend;
use App\Entity\Shop;
use App\Events;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncode;

class EmailController extends AbstractController
{
    protected $manager;
    protected $eventDispatcher;

    public function __construct(ObjectManager $manager, EventDispatcherInterface $eventDispatcher)
    {
        $this->manager = $manager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Retourne la liste de tous les mails envoyés
     * @Route("/api/email_sends", name="get_emails", methods={"GET"})
     * @return mixed
     */
    public function getEmails()
    {
        $emails = $this->getDoctrine()->getRepository('App:EmailSend')->findAll();
        if ($emails) {
            // Serialize les objets au format Json
            $emails = $this->get('serializer')->serialize($emails, 'json');
            return new JsonResponse($emails, Response::HTTP_OK, [], true);
        } else {
            return new JsonResponse("Bad Request", Response::HTTP_BAD_REQUEST, [], true);
        }
    }

    /**
     * Methode qui appel un service qui envoie un email pour pres inscrire le compte.
     * Le compte est en attente de validation par l'admin
     * @param $account
     * @throws \Exception
     */
    public function user_register_waiting($account)
    {
        $date = new DateTime();
        $date->getTimestamp();
        //On envoie l'email
        $event = new GenericEvent($account);
        $this->eventDispatcher->dispatch(Events::USER_REGISTERED, $event);
        //On inscrit l'email dans la table Email_Send
        $email_send = new EmailSend();
        $email_send->setStatus('send')
            ->setType('waiting')
            ->setCreatedAt($date)
            ->setAccount($account);
        $this->manager->persist($email_send);
    }

    /**
     * Methode qui appel un service qui envoie un email suite a la valisation du compte.
     * @param $account
     * @throws \Exception
     */
    public function user_register_validate($account)
    {
        $date = new DateTime();
        $date->getTimestamp();
        //On envoie l'email
        $event = new GenericEvent($account);
        $this->eventDispatcher->dispatch(Events::USER_REGISTERED_VALIDATE, $event);
        //On inscrit l'email dans la table Email_Send
        $email_send = new EmailSend();
        $email_send->setStatus('send')
            ->setType('validate')
            ->setCreatedAt($date)
            ->setAccount($account);
        $this->manager->persist($email_send);
        $this->manager->flush();
    }
}
