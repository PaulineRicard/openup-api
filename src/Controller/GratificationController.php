<?php

namespace App\Controller;

use App\Entity\Gratification;
use App\Entity\Sponsor;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GratificationController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * MessageController constructor.
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     */
    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    /**
     * Création d'une gratification
     * @Route("/api/gratification/{sponsor_id}", methods={"POST"})
     * @param $sponsor_id
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return Response
     */
   public function createGratification($sponsor_id, Request $request, SerializerInterface $serializer, ObjectManager $manager)
   {
       $data = $request->getContent();
       $dataTab = $serializer->decode($data, 'json');

       $sponsor = $manager->getRepository(Sponsor::class)->find($sponsor_id);

       $gratification = new Gratification();
       $gratification->setName($dataTab['name'])
                     ->setDescription($dataTab['description'])
                     ->setAmount($dataTab['amount'])
                     ->setQuantity($dataTab['quantity'])
                     ->setSponsor($sponsor);

       $this->manager->persist($gratification);
       $this->manager->flush();

       return new Response('Gratification bien enregistrée');
   }

    /**
     * Retourne une gratification par son id passée en paramètre
     * @param $id
     */
   public function getGratification($id)
   {
       $gratification = $this->getDoctrine()
           ->getRepository(Gratification::class)
           ->find($id);

       if(!$gratification) {
           throw $this->createNotFoundException(
               'Gratification non trouvée'
           );
       }

   }

    /**
     * Retourne la liste des gratifications
     */
   public function getGratifications()
   {
       $gratifications = $this->getDoctrine()
           ->getRepository(Gratification::class)
           ->findAll();

       if(!$gratifications) {
           throw $this->createNotFoundException(
               'Erreur, liste non trouvée'
           );
       }

   }

    /**
     * Modification d'une gratification
     * @Route("/api/gratifications/{id}", methods={"PUT"})
     * @param $id
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return JsonResponse
     */
   public function updateGratification($id, Request $request, SerializerInterface $serializer, ObjectManager $manager)
   {
       $data = $request->getContent();
       $dataTab = $serializer->decode($data, 'json');

       $gratification = $this->getDoctrine()->getRepository(Gratification::class)->find($id);

       if(!$gratification) {
           throw $this->createNotFoundException(
               'Gratification non trouvée'
           );
       }

       $gratification->setName($dataTab['name'])
           ->setDescription($dataTab['description'])
           ->setAmount($dataTab['amount'])
           ->setQuantity($dataTab['quantity']);

       $this->manager->flush();

       return new JsonResponse('{"message":"Gratification mise à jour","status":"201"}', Response::HTTP_CREATED, [], true);
   }

    /**
     * Supprime une gratification
     */
   public function deleteGratification($id)
   {
       $entityManager = $this->getDoctrine()->getManager();
       $gratification = $entityManager->getRepository(Gratification::class)->find($id);

       if(!$gratification) {
           throw $this->createNotFoundException(
               'Gratification non trouvée'
           );
       }

       $entityManager->remove($gratification);
       $entityManager->flush();
   }
}
