<?php

namespace App\Controller;

use App\Entity\Trader;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class TraderController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $manager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * SponsorController constructor.
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     */
    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    /**
     * Méthode appelé lors de la creation d'un compte user
     * @param $account
     * @param $dataTab
     * @param $type
     * @return Trader
     */
    public function create($account, $dataTab, $type)
    {
        $trader = new Trader();
        $trader ->setSiret($dataTab['siret'])
                ->setAccount($account);
        $this->manager->persist($trader);
        $this->manager->flush();

        $shop = new ShopController($this->serializer,$this->manager);
        $shop->create($trader, $dataTab, $type);

        return $trader;
    }

    /**
     * Retourne l'account par l'id du trader, requete sql dans le TraderRepository
     * @Route("/api/traders/{id}/account")
     * @param $id
     * @return object[]
     */
    public function getTraderAcc($id)
    {
        $trader = $this->getDoctrine()->getRepository(Trader::class)->getTraderAccount($id);
        dd($trader);
        return new JsonResponse($trader);
    }

}
