<?php

namespace App\Controller;

use App\Entity\Stripe;
use App\Entity\Wallet;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WalletController extends AbstractController
{
    /**
     * Inscrit la transaction dans la table stripe
     * @Route("api/wallet/stripe", name="wallet_stripe", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return JsonResponse
     * @throws \Exception
     */
    public function createStripeTransaction(Request $request, ValidatorInterface $validator, SerializerInterface $serializer, ObjectManager $manager)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        // recupere l'objet User
        $user = $this->getDoctrine()->getRepository('App:User')->find($dataTab['user']);

        $date = new DateTime();
        $date->getTimestamp();

        $stripe = new Stripe();
        $stripe->setAmount($dataTab['amount'])
            ->setIdTransactionStripe($dataTab['idTransactionStripe'])
            ->setIdCustomerStripe($dataTab['idCustomerStripe'])
            ->setCreatedAt($date)
            ->setUser($user);

        $manager->persist($stripe);
        $manager->flush();

        $this->updateWBalanceWallet($dataTab, $manager, $user, $origin = "plus");
        return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);
    }


    /**
     *  Update la table wallet_balance suite a un credit ou un debit
     * @param $dataTab
     * @param ObjectManager $manager
     * @param $user
     * @param $origin
     */
    public function updateWBalanceWallet($dataTab, $manager, $user, $origin)
    {
        //$user = $this->getDoctrine()->getRepository('App:User')->find($dataTab['user']);
        $balanceWallet = $user->getBalanceWallet();
        if ($origin == "plus") {
            $balanceWallet += $dataTab['amount'];
        } else if ($origin == "moins") {
            $balanceWallet -= $dataTab['amount'];
        }
        $user->setBalanceWallet($balanceWallet);
        $manager->persist($user);
        $manager->flush();
    }


    /**
     * Methode appelé lors de la creation de la valiadtion d'une commande
     * @param $user
     * @param $amount
     * @param $date
     * @param ObjectManager $manager
     */
    public function create($user, $amount, $date, ObjectManager $manager)
    {
        $wallet = new Wallet();
        $wallet ->setAmount($amount)
            ->setCreatedAt($date)
            ->setUser($user);
        $manager->persist($wallet);
        $manager->flush();
    }
}
