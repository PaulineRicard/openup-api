<?php

namespace App\Controller;

use App\Entity\Order;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class OrderController extends AbstractController
{
    /**
     * @Route("api/order/create", name="create_order", methods={"POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $request, ValidatorInterface $validator, SerializerInterface $serializer, ObjectManager $manager)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        // Retrieve User Entity
        $user = $this->getDoctrine()->getRepository('App:User')->find($dataTab['user_id']);

        $date = new DateTime();
        $date->getTimestamp();

        $order = new Order();
        $order->setCreatedAt($date);
        $order->setStatus('waiting');
        $order->setAmount($dataTab['amount']);
        $order->setQrcode('Order_');
        $order->setUser($user);
        $manager->persist($order);
        $manager->flush();

        //update du QrCode avec le numero de la commande
        $orderId = $order->getId();
        $order->setQrcode('Order_' . $orderId);
        $manager->persist($order);
        $manager->flush();

        $this->validateOrder($dataTab, $manager, $user);

        //Inscrit une nouvelle ligne dans la table Wallet
        $user = $order->getUser();
        $amount = -$dataTab['amount'];
        $createWallet = new WalletController();
        $createWallet->create($user, $amount, $date, $manager);

        //Gestion des erreurs de validation
        $errors = $validator->validate($order);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }

        return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);

    }

    public function validateOrder($dataTab, ObjectManager $manager, $user)
    {
        $wallet = new WalletController();
        $wallet->updateWBalanceWallet($dataTab, $manager, $user, $origin = "moins");
    }


    /**
     * Permet de rembourser un paiement
     * @route("/api/order/refund/{id}")
     * @param $id
     * @param ObjectManager $manager
     * @param ValidatorInterface $validator
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function refundOrder($id, ObjectManager $manager, ValidatorInterface $validator, SerializerInterface $serializer)
    {
        //Update order status en "refund"
        $order = $this->getDoctrine()->getRepository('App:Order')->find($id);
        $order->setStatus('refund');
        $manager->persist($order);

        // Recredite le wallet-balance dans user de la somme de la commande
        $user = $this->getDoctrine()->getRepository('App:User')->find($order->getUser());
        $user->setBalanceWallet($order->getAmount());
        $manager->persist($user);
        $manager->flush();

        //Inscrit une nouvelle ligne dans la table Wallet
        $amount = $order->getAmount();
        $date = $order->getCreatedAt();
        $createWallet = new WalletController();
        $createWallet->create($user, $amount, $date, $manager);

        // gestion des erreurs de validation
        $errors = $validator->validate($order);
        if (count($errors)) {
            $errorsJson = $serializer->serialize($errors, 'json');
            return new JsonResponse($errorsJson, Response::HTTP_BAD_REQUEST, [], true);
        }
        return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);
    }

    /**
     * Recupere less commande
     * @Route("/api/orders")
     * @return mixed
     */
    public function getOrders()
    {
        $orders = $this->getDoctrine()->getRepository(Order::class)->getAllInOrder();
        if (!$orders) {
            throw $this->createNotFoundException('commande non trouvée');
        }
        return new JsonResponse($orders);
    }

    /**
     * Permet a un user d'accepter une livraison
     * @Route("/api/order/{order_id}/accepted/{user_id}", name="accept_order", methods={"POST"})
     * @param $order_id
     * @param $user_id
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function AcceptedOrder($order_id, $user_id, ObjectManager $manager)
    {
        //Selection du user qui accepte la livraison
        $account = $manager->getRepository('App:Account')->find($user_id);
        $user = $manager->getRepository('App:User')->find($account->getId());

        //Selection de la commande
        $order = $manager->getRepository('App:Order')->find($order_id);

        $OrderStatus = $order->getStatus();
        $role = $account->getRole();

        if ($OrderStatus == "waiting" && $order->getUser() == null && $role == "user") {
            $order->setUser($user);
            $order->setStatus('accepted');
        } else {
            return new JsonResponse('{"message":"Unauthorized","status":"401"}', Response::HTTP_UNAUTHORIZED, [], true);
        }
        $manager->persist($order);
        $manager->flush();
        return new JsonResponse('{"message":"Created","status":"201"}', Response::HTTP_CREATED, [], true);
    }

    /**
     * Permet de modifier le status en fonction de la personne qui flash le QR Code
     * @Route("/api/order/{user_id}/{$order_id}/status", name="modified_status_order", methods={"POST"})
     * @param $user_id
     * @param $order_id
     * @param ObjectManager $manager
     * @return JsonResponse $manager
     */
    public function ModifiedStatusOrder($user_id, $order_id, ObjectManager $manager)
    {
        //Selection du user qui flash le qr-code
        $account = $manager->getRepository('App:Account')->find($user_id);

        //Selection de la commande
        $order = $manager->getRepository('App:Order')->find($order_id);

        $OrderStatus = $order->getStatus();

        if ($OrderStatus == "accepted" || $OrderStatus == "pickup") {
            $role = $account->getRole();
            if ($role == "trader") {
                $order->setStatus('pickup');
            } else if ($role == "user") {
                $order->setStatus('closed');
            } else {
                return new JsonResponse('{"message":"Unauthorized","status":"401"}', Response::HTTP_UNAUTHORIZED, [], true);
            }
        } else {
            return new JsonResponse('{"message":"Unauthorized","status":"401"}', Response::HTTP_UNAUTHORIZED, [], true);
        }
        $manager->persist($order);
        $manager->flush();
        return new JsonResponse('{"message":"Created","status":"201"}', Response::HTTP_CREATED, [], true);
    }


    /**
     * Permet de recurere les commandes terminés par le user livré
     * @Route("/api/order/closed/orderer/{id}", name="order_closed_by_orderer", methods={"GET"})
     * @param $id
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function getOrderClosedByOrderer($id, ObjectManager $manager)
    {
        $account = $manager->getRepository('App:Account')->find($id);
        $order = $manager->getRepository('App:Order')->findBy(["wallet"=>$account, "status"=> "closed"]);
        return new JsonResponse(count($order), 200);
    }

    /**
     * Permet de recurere les commandes terminés par le user livreur
     * @Route("/api/order/closed/deliverer/{id}", name="order_closed_by_deliverer", methods={"GET"})
     * @param $id
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function getOrderClosedByDeliverer($id, ObjectManager $manager)
    {
        $account = $manager->getRepository('App:Account')->find($id);
        $order = $manager->getRepository('App:Order')->findBy(["user"=>$account, "status"=> "closed"]);
        return new JsonResponse(count($order), 200);
    }
}
