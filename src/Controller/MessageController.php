<?php

namespace App\Controller;

use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Entity\Conversation;
use App\Entity\Message;
use DateTime;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class MessageController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;


    /**
     * MessageController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * Envoie le premier message SAV du user à l'admin ID 1
     * @Route("/api/message/{user_id}", name="create_message", methods={"POST"})
     * @param $user_id
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return Response
     * @throws \Exception
     */
    public function create($user_id, Request $request, SerializerInterface $serializer, ObjectManager $manager)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');
        $date = new DateTime();
        $date->getTimestamp();

        if ($user_id && $dataTab['conversation_id'] === null) {
            // Permier message du user à l'admin si l'id de la comversation est Null
            $conversation = new Conversation();
            $conversation->setAdminId('1')
                ->setUserId($user_id);
            $manager->persist($conversation);
            $manager->flush();
            // Inscrit un nouveau message
            $message = new Message();
            $message->setContent($dataTab['content'])
                ->setStatusUser("read")
                ->setStatusAdmin("unread")
                ->setCreatedAt($date)
                ->setConversation($conversation);
            $manager->persist($message);
            $manager->flush();
            return new JsonResponse('{"message":"Message envoyé","status":"201"}', Response::HTTP_CREATED, [], true);
        } else if ($user_id && $dataTab['conversation_id'] !== null) {
            // Réponse du user à l'admin grace a l'Id de la conversation
            $conversation = $manager->getRepository('App:Conversation')->findby(["id" => $dataTab['conversation_id']]);
            if ($conversation[0]->getUserId() == intval($user_id)) {
                $message = new Message();
                $message->setContent($dataTab['content'])
                    ->setStatusUser("read")
                    ->setStatusAdmin("unread")
                    ->setCreatedAt($date)
                    ->setConversation($conversation[0]);
                $manager->persist($message);
                $manager->flush();
                return new JsonResponse('{"message":"Réponse envoyée","status":"201"}', Response::HTTP_CREATED, [], true);
            } else {
                return new JsonResponse('{"message":"Unauthorized","status":"401"}', Response::HTTP_UNAUTHORIZED, [], true);
            }
        } else {
            return new JsonResponse('{"message":"Bad Request","status":"400"}', Response::HTTP_BAD_REQUEST, [], true);
        }
    }

    /**
     * Réponse de l'admin a un user grace a l'Id de la conversation
     * @Route("/api/message/{conv_id}/admin", methods={"POST"})
     * @param $conv_id
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return Response
     * @throws \Exception
     */
    public function adminMessageResponse($conv_id, Request $request, SerializerInterface $serializer, ObjectManager $manager)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        $date = new DateTime();
        $date->getTimestamp();
        $conversation = $manager->getRepository('App:Conversation')->findby(["id" => $conv_id]);

        $message = new Message();
        $message->setContent($dataTab['content'])
            ->setStatusUser("unread")
            ->setStatusAdmin("read")
            ->setCreatedAt($date)
            ->setConversation($conversation[0]);
        $manager->persist($message);
        $manager->flush();
        return new JsonResponse('{"message":"Réponse envoyée","status":"201"}', Response::HTTP_CREATED, [], true);
    }

    /**
     * Recupere les messages d'une meme conversation
     * @Route("/api/messages/{conv_id}", name="all_messages_by_conversation", methods={"GET"})
     * @param $conv_id
     * @param ObjectManager $manager
     * @return Response
     */
    public function getAllMessagesByConversation($conv_id, ObjectManager $manager)
    {
        $messages = $manager->getRepository('App:Message')->findBy(['conversation' => $conv_id]);
        $messages = $this->get('serializer')->serialize($messages, 'json');
        return new JsonResponse($messages, Response::HTTP_OK, [], true);
    }

    /**
     * retourne tous les messages non lu de l'admin
     * @Route("/api/messages/new/admin", name="all_new_messages_for_admin", methods={"GET"})
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function getAllNewsAdminMessages( ObjectManager $manager)
    {
        $messages = $manager->getRepository(Message::class)->findBy(['status_admin' => 'unread']);
        $messages = $this->get('serializer')->serialize($messages, 'json');
        return new JsonResponse($messages, Response::HTTP_OK, [], true);
    }

    /**
     * Récupere la liste des conversations
     * @Route("/api/conversations", methods={"GET"})
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function getAllConversations(ObjectManager $manager)
    {
        $conversation = $manager->getRepository('App:Message')->findAll();
        $messages = $this->get('serializer')->serialize($conversation, 'json');
        return new JsonResponse($messages, Response::HTTP_OK, [], true);
    }


    /**
     * Permet d'update le statut du message
     * @Route("/api/message/{msg_id}/status/{status_admin}", methods={"POST"})
     * @param $msg_id
     * @param $status_admin
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function updateMessageStatus($msg_id, $status_admin, ObjectManager $manager)
    {
        $messages = $manager->getRepository('App:Message')->findBy(['id' => $msg_id]);
        $messages[0]->setStatusAdmin($status_admin);

        $manager->persist($messages[0]);
        $manager->flush();
        return new JsonResponse('{"message":"Status modifié","status":"201"}', Response::HTTP_CREATED, [], true);
    }

}
