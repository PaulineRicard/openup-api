<?php

namespace App\Controller;

use App\Entity\ShopType;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ShopTypeController extends AbstractController
{
    /**
     * Crée les types de commerce
     * @Route("/api/shop_types", name="shop_type", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @return Response
     */
    public function createShopType(Request $request, SerializerInterface $serializer, ObjectManager $manager): Response
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');
        $shopType = new ShopType();
        $shopType->setName($dataTab['name']);
        $manager->persist($shopType);
        $manager->flush();
        return new Response('Type de boutique créé');
    }

    /**
     * @Route("/api/shop_types/{id}", name="shop_type_id", methods={"PUT"})
     * @param $id
     * @param SerializerInterface $serializer
     * @param ObjectManager $manager
     * @param Request $request
     * @return Response
     */
    public function update($id, SerializerInterface $serializer, ObjectManager $manager, Request $request)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');
        $shopType = $manager->getRepository('App:ShopType')->find($id);
        $shopType->setName($dataTab['name']);
        $manager->persist($shopType);
        $manager->flush();
        return new Response("Modification ok", Response::HTTP_CREATED);
    }
}
