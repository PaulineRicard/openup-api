<?php

namespace App\Controller;

use App\Entity\Item;
use App\Entity\Shop;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ItemController extends AbstractController
{
    private $manager;
    private $serializer;

    public function __construct(SerializerInterface $serializer, ObjectManager $manager)
    {
        $this->serializer = $serializer;
        $this->manager = $manager;
    }

    /**
     * Création d'un article
     * @Route("api/items", name="item", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @param $id
     * @return JsonResponse
     */
    public function create(Request $request, SerializerInterface $serializer)
    {
        $data = $request->getContent();
        $dataTab = $serializer->decode($data, 'json');

        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($dataTab['shop']);

        $item = new Item();
        $item->setName($dataTab['name'])
            ->setDescription($dataTab['description'])
            ->setPrice($dataTab['price'])
            ->setShop($shop);

        $this->manager->persist($item);
        $this->manager->flush();
        return new JsonResponse("creation de l'article ok", Response::HTTP_CREATED, [], true);
    }

    /**
     * Mise à jour d'un article
     * @param $id
     * @return JsonResponse
     */
    public function update($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $item = $entityManager->getRepository(Item::class)->find($id);

        if(!$item) {
            throw $this->createNotFoundException(
                'Article non trouvé'
            );
        }

        $item->setName('name')
            ->setDescription('description')
            ->setPrice('price');
        $entityManager->flush();
        return new JsonResponse('Modification de l\'article '.$id. 'ok', Response::HTTP_CREATED, [], true);
    }

}
