<?php

namespace App\Controller;

use App\Entity\Document;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DocumentController extends AbstractController
{

    /**
     * Recupere la photo de profil / avatar du user
     * @Route("api/document/{user_id}/avatar", name="document", methods={"POST"})
     * @param $user_id
     * @param Request $request
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function avatarUploadAction($user_id, Request $request, ObjectManager $manager)
    {
        // On récupère une instance de UploadedFile, à partir de la requête.
        $uploadedFile = $request->files->get('file');
        $document = new Document();
        if ($uploadedFile) {
            $newFilename = 'avatar-' . $user_id . '.' . $uploadedFile->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $uploadedFile->move($this->getParameter('avatar_directory'), $newFilename);
            } catch (FileException $e) {
                return new JsonResponse("ca a loupé", Response::HTTP_BAD_REQUEST, [], true);
            }
            $user = $manager->getRepository('App:User')->find($user_id);

            $document->setName($newFilename)
                ->setPath("public/images/users/" . $newFilename)
                ->setUser($user);
            $manager->persist($document);
            $manager->flush();
            return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);
        }
    }

    /**
     * Recupere la photo de la carte d'identité du user
     * @Route("api/document/{user_id}/cni", name="document", methods={"POST"})
     * @param $user_id
     * @param Request $request
     * @param ObjectManager $manager
     * @return JsonResponse
     */
    public function CniUploadAction($user_id, Request $request, ObjectManager $manager)
    {
        // On récupère une instance de UploadedFile, à partir de la requête.
        $uploadedFile = $request->files->get('file');
        $document = new Document();
        if ($uploadedFile) {
            $newFilename = 'avatar-' . $user_id . '-cni.' . $uploadedFile->guessExtension();
            // Move the file to the directory where brochures are stored
            try {
                $uploadedFile->move($this->getParameter('cni_directory'), $newFilename);
            } catch (FileException $e) {
                return new JsonResponse("ca a loupé", Response::HTTP_BAD_REQUEST, [], true);
            }
            $user = $manager->getRepository('App:User')->find($user_id);

            $document->setName($newFilename)
                ->setPath("images/users_cni/" . $newFilename)
                ->setUser($user);
            $manager->persist($document);
            $manager->flush();
            return new JsonResponse('{"message":"creation ok","status":"201"}', Response::HTTP_CREATED, [], true);
        }
    }
}
