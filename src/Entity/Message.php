<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessageRepository")
 * @ApiResource(
 *     collectionOperations={
            "get"={"path"="/messages/unread"}
 *     }
 * )
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255)
     * @Column(type="string", columnDefinition="ENUM('read', 'unread', 'archived')")
     * @ApiSubresource()
     */
    private $status_user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Column(type="string", columnDefinition="ENUM('read', 'unread', 'archived')")
     * @ApiSubresource()
     */
    private $status_admin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Conversation", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $conversation;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getStatusUser(): ?string
    {
        return $this->status_user;
    }

    public function setStatusUser(string $status_user): self
    {
        $this->status_user = $status_user;

        return $this;
    }

    public function getStatusAdmin(): ?string
    {
        return $this->status_admin;
    }

    public function setStatusAdmin(string $status_admin): self
    {
        $this->status_admin = $status_admin;

        return $this;
    }

    public function getConversation(): ?Conversation
    {
        return $this->conversation;
    }

    public function setConversation(?Conversation $conversation): self
    {
        $this->conversation = $conversation;

        return $this;
    }

}
