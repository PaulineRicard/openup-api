<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmailSendRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"=
 *                  {
 *                      "groups"={"get_account_role_admin"}
 *                  }
 *           }
 *     }
 * )
 */
class EmailSend
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get_account_role_admin"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Column(type="string", columnDefinition="ENUM('send', 'error')")
     * @Groups({"get_account_role_admin"})
     */
    private $status;

    const TYPE_WAITING = 'waiting';
    const TYPE_VALIDATE = 'validate';
    const TYPE_BILL = 'bill';
    const TYPE_REFUSED = 'refused';
    const TYPE_RELOAD = 'reload';
    const TYPE_BLOCKED = 'blocked';
    const TYPE_GRATS = 'grats';

    /**
     * @ORM\Column(type="string", length=255)
     * @Column(type="string", columnDefinition="ENUM('waiting', 'validate', 'bill', 'refused', 'reload', 'blocked', 'grats')")
     * @Groups({"get_account_role_admin"})
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"get_account_role_admin"})
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"get_account_role_admin"})
     */
    private $account;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, array(self::TYPE_REFUSED, self::TYPE_WAITING, self::TYPE_VALIDATE, self::TYPE_BLOCKED, self::TYPE_BILL, self::TYPE_GRATS, self::TYPE_RELOAD))) {
            throw new \InvalidArgumentException("Invalid status");
        }
        $this->type = $type;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

}
