<?php

namespace App\DataFixtures;

use App\Entity\Account;
use App\Entity\Address;
use App\Entity\Admin;
use App\Entity\Document;
use App\Entity\EmailSend;
use App\Entity\Item;
use App\Entity\ItemOrder;
use App\Entity\Message;
use App\Entity\Order;
use App\Entity\Shop;
use App\Entity\ShopType;
use App\Entity\Sponsor;
use App\Entity\Trader;
use App\Entity\User;
use App\Entity\Wallet;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Mpdf\Css\Border;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $manager;
    private $passwordEncoder;
    private $wallet;
    private $item1;
    private $date;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, ObjectManager $manager)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->manager = $manager;
        $this->date = new DateTime();
        $this->date->getTimestamp();
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->loadAdmin();
        $this->loadShopType();
        $this->loadUserOrderer();
        $this->loadUserDeliverer();
        $this->loadSponsor();
        $this->loadTrader();
        $this->loadOrder();
    }

    /**
     *creation des adherents
     */
    public function loadAdmin()
    {
        $admin = new Admin();
        $admin->setEmail("admin@openup.com")
            ->setPassword($this->passwordEncoder->encodePassword($admin, "admin"));
        $this->manager->persist($admin);

        $this->manager->flush();
    }

    /**
     *creation d'un user qui passe la commande
     */
    public function loadUserOrderer()
    {
        $address = new Address();
        $address->setAddress('10 avenue Jean Moulin');
        $address->setCity('Béziers');
        $address->setZipcode('34500');
        $this->manager->persist($address);
        $this->manager->flush();

        $account = new Account();
        $account->setFirstname('Jean')
            ->setLastname('ORDERERMAN')
            ->setEmail("orderer@openup.com")
            ->setRole('user')
            ->setStatus('waiting')
            ->setPassword($this->passwordEncoder->encodePassword($account, 'user'))
            ->setPhone('0651532351')
            ->setAddress($address);
        $this->manager->persist($account);
        $this->manager->flush();

        $user = new User();
        $user->setBalanceWallet('10')
            ->setXCoordinate('3.22045')
            ->setYCoordinate('43.347112')
            ->setAccount($account);
        $this->manager->persist($user);
        $this->manager->flush();



        $wallet = new Wallet();
        $wallet->setCreatedAt($this->date)
            ->setAmount('10')
            ->setUser($user);
        $this->manager->persist($wallet);
        $this->manager->flush();

        $this->wallet = $wallet;

        $document = new Document();
        $document->setName("avatar-1")
            ->setPath("images/users/avatar-1.jpg")
            ->setUser($user);
        $this->manager->persist($document);
        $this->manager->flush();
        $document = new Document();
        $document->setName("avatar-1-cni")
            ->setPath("images/users_cni/avatar-1-cni.jpg")
            ->setUser($user);
        $this->manager->persist($document);
        $this->manager->flush();

        $email = new EmailSend();
        $email->setAccount($account)
            ->setCreatedAt($this->date)
            ->setStatus("send")
            ->setType("waiting");
        $this->manager->persist($email);
        $this->manager->flush();


//        //id	admin_id	user_id	title	created_at	status_user	status_admin
//        $admin = $this->manager->getRepository('App:Admin')->find('1');
//        $msg = new Message();
//        $msg->setCreatedAt($this->date)
//            ->setTitle("On en a gros !")
//            ->setAdmin($admin)
//            ->setUser($user)
//            ->setStatusUser("unread")
//            ->setStatusAdmin("unread");
//        $this->manager->persist($msg);
//        $this->manager->flush();
    }


    /**
     *creation d'un user livreur
     */
    public function loadUserDeliverer()
    {
        $address = new Address();
        $address->setAddress('8 avenue Clemenceau');
        $address->setCity('Béziers');
        $address->setZipcode('34500');
        $this->manager->persist($address);
        $this->manager->flush();

        $account = new Account();
        $account->setFirstname('Gil')
            ->setLastname('DELIVERMAN')
            ->setEmail("deliverman@openup.com")
            ->setRole('user')
            ->setStatus('waiting')
            ->setPassword($this->passwordEncoder->encodePassword($account, 'user'))
            ->setPhone('0665458547')
            ->setAddress($address);
        $this->manager->persist($account);
        $this->manager->flush();

        $user = new User();
        $user->setBalanceWallet('0')
            ->setXCoordinate('3.216034')
            ->setYCoordinate('43.345202')
            ->setAccount($account);
        $this->manager->persist($user);
        $this->manager->flush();

        $document = new Document();
        $document->setName("avatar-2")
            ->setPath("images/users/avatar-2.png")
            ->setUser($user);
        $this->manager->persist($document);
        $this->manager->flush();
        $document = new Document();
        $document->setName("avatar-2-cni")
            ->setPath("images/users_cni/avatar-2-cni.jpg")
            ->setUser($user);
        $this->manager->persist($document);
        $this->manager->flush();

        $email = new EmailSend();


        $email->setAccount($account)
            ->setCreatedAt($this->date)
            ->setStatus("send")
            ->setType("waiting");
        $this->manager->persist($email);
        $this->manager->flush();

    }

    /**
     *creation des Shop Type
     */
    public function loadShopType()
    {
        $type = ["Boulangerie", "Primeur", "Epicerie", "Droguerie", "Patisserie", "Chocolaterie", "Fromager", "Caviste"];

        for ($i = 0; $i < count($type); $i++) {
            $shopType = new ShopType();
            $shopType->setName($type[$i]);
            $this->manager->persist($shopType);
            $this->manager->flush();
        }
    }

    /**
     *creation des Sponsors
     */
    public function loadSponsor()
    {
        $address = new Address();
        $address->setAddress('Place Gabriel Péri');
        $address->setCity('Béziers');
        $address->setZipcode('34500');
        $this->manager->persist($address);
        $this->manager->flush();

        $account = new Account();
        $account->setFirstname('Boby')
            ->setLastname('SPONSORMAN')
            ->setEmail("sponsor@openup.com")
            ->setRole('sponsor')
            ->setStatus('validate')
            ->setPassword($this->passwordEncoder->encodePassword($account, 'sponsor'))
            ->setPhone('0685545654')
            ->setAddress($address);
        $this->manager->persist($account);
        $this->manager->flush();

        $sponsor = new Sponsor();
        $sponsor->setName('Mairie de Béziers')
            ->setAccount($account);
        $this->manager->persist($sponsor);
    }

    public function loadTrader()
    {
        $address = new Address();
        $address->setAddress('66 avenue Jeam Moulin');
        $address->setCity('Béziers');
        $address->setZipcode('34500');
        $this->manager->persist($address);
        $this->manager->flush();

        $account = new Account();
        $account->setFirstname('Robert')
            ->setLastname('TRADERMAN')
            ->setEmail("trader@openup.com")
            ->setRole('trader')
            ->setStatus('waiting')
            ->setPassword($this->passwordEncoder->encodePassword($account, 'trader'))
            ->setPhone('0765456564')
            ->setAddress($address);
        $this->manager->persist($account);
        $this->manager->flush();

        $trader = new Trader();
        $trader->setSiret('123456789100')
            ->setAccount($account);
        $this->manager->persist($trader);
        $this->manager->flush();

        $type = $this->manager->getRepository('App:ShopType')->find('2');

        $shop = new Shop();
        $shop->setName('Boulangerie Mesouilles')
            ->setShopType($type)
            ->setXCoordinate('3.227479')
            ->setYCoordinate('43.350489')
            ->setTrader($trader);

        $this->manager->persist($shop);
        $this->manager->flush();

        $item1 = new Item();
        $item1->setName('Baguette')
            ->setDescription('Baguette de pain blanc - 150gr')
            ->setPrice('1.1')
            ->setShop($shop);
        $this->manager->persist($item1);
        $this->manager->flush();
        $this->item1 = $item1;

        $item2 = new Item();
        $item2->setName('Croissant')
            ->setDescription('Croissant au beurre')
            ->setPrice('0.80')
            ->setShop($shop);
        $this->manager->persist($item2);
        $this->manager->flush();

        $item3 = new Item();
        $item3->setName('Pain au chocolat')
            ->setDescription('Pain au chocolat au beurre')
            ->setPrice('0.90')
            ->setShop($shop);
        $this->manager->persist($item3);
        $this->manager->flush();
    }


    /**
     *creation d'une commande
     */
    public function loadOrder()
    {
        $order = new Order();
        $order->setCreatedAt($this->date)
            ->setStatus('waiting')
            ->setAmount('0')
            ->setQrcode('Order_1')
            ->setWallet($this->wallet);
        $this->manager->persist($order);
        $this->manager->flush();

        //	id	item_id	order_id	quantity
        $item_order = new ItemOrder();
        $item_order->setOrder($order)
            ->setItem($this->item1)
            ->setQuantity(2);
        $this->manager->persist($item_order);
        $this->manager->flush();

        $quantity = $item_order->getQuantity();
        $price = $item_order->getItem()->getPrice();

        $order->setAmount($quantity * $price);
        $this->manager->persist($order);
        $this->manager->flush();

    }


}
