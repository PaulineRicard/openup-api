<?php

// App\EventListener\RegistrationNotifySubscriber.php
namespace App\EventListener;

    use App\Entity\Account;
    use App\Events;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\EventDispatcher\GenericEvent;

    /**
     * Envoi un mail de validation pour la creation d'un compte utilisateur
     */
class RegistrationValidationSubscriber implements EventSubscriberInterface
{

    private $mailer;
    private $sender;

    public function __construct(\Swift_Mailer $mailer, $sender)
    {
        // On injecte notre expediteur et la classe pour envoyer des mails
        $this->mailer = $mailer;
        $this->sender = $sender;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            // le nom de l'event et le nom de la fonction qui sera déclenché
            Events::USER_REGISTERED_VALIDATE => 'onUserValidate',
        ];
    }

    public function onUserValidate(GenericEvent $event): void
    {
        /** @var Account $user */
        $user = $event->getSubject();


        // Create the Transport
        $transport = new \Swift_SmtpTransport('smtp.mailgun.org', 587);
        $transport->setUsername('postmaster@sandboxedb503dabff543cdbc10eb7dab74f591.mailgun.org')
            ->setPassword('8552e133ac3da3bb1ed18e57c3d79a03-9c988ee3-900bfdda');

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);

        // Create a message
        $subject = "User Account Validate !";
        $body = "Bienvenue chez Open UP mec !";

        $message = new \Swift_Message($subject);
        $message->setFrom(array( $this->sender => 'Admin OpenUp'))
            ->setTo(array($user->getEmail()))
            ->setBody($body);

        // Send the message
        $mailer->send($message);
    }
}
