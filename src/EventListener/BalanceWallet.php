<?php

namespace App\EventListener;

use App\Entity\Stripe;
use App\Entity\Wallet;
use App\Entity\User;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\HttpKernel\KernelEvents;


class BalanceWallet{

    private $stripeAmount;

    public static function getAllAmount()
    {
        return [
            KernelEvents::VIEW => ['setUser', EventPriorities::POST_VALIDATE],
        ];


    }

    public function getStripeAmount(ViewEvent $event)
    {
        $user = $event->getControllerResult();

      if($user instanceof Stripe && $user->getAmount()) {
            // stripe - wallet/user_id = setBalanceWallet

          return $this->stripeAmount = $user->getAmount();


        }
    }

    public function setBalanceWallet(ViewEvent $event)
    {
        $user = $event->getControllerResult();

      if($user instanceof User && $user->getBalanceWallet()) {
            // stripe - wallet/user_id = setBalanceWallet
            $walletBalance = $user->getBalanceWallet();
            $stripeAmount = $this->getStripeAmount($event);

          $user->setBalanceWallet($walletBalance + $stripeAmount);

        }
    }

}
