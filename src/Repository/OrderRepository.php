<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getAllInOrder(): array
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql =
            'SELECT DISTINCT `order`.`id`, `order`.`created_at`, `order`.`status`, `order`.`amount`, `order`.`qrcode`,
                    shop.id as shopId, shop.name as shopName, shop.x_coordinate as shopXCoordinate, shop.y_coordinate as shopYCoordinate,
                    item.id as itemId, item.name as itemName, item.description as itemDescription, item.price as itemPrice,
                    item_order.quantity as quantity,
                    trader.id as traderId, trader.siret as traderSiret,
                    wallet.id as walletId, wallet.user_id as deliveryId, wallet.amount as walletAmout,
                    `order`.`user_id` as deliveryManId
                    from `order`
                    JOIN item_order ON `order`.`id` = item_order.order_id
                    JOIN wallet ON `order`.`wallet_id` = wallet.id
                    JOIN item ON item.id = item_order.item_id
                    JOIN shop ON shop.id = item.shop_id
                    JOIN trader ON trader.id = shop.trader_id';

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
