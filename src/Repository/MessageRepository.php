<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;

/**
 * @method Message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Message[]    findAll()
 * @method Message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

//    /**
//     * @return array
//     * @throws DBALException
//     */
//    public function getAllMessagesByConversation(): array
//    {
//        $conn = $this->getEntityManager()->getConnection();
//
//        $sql = 'select message.id as message_id, message.content as content, message.created_at as created_at, message.status_admin as status, message.conversation_id as conversation_id, user_conversation.user_id as user_id, user.account_id as user_account_id, account.firstname as firstname, account.lastname as lastname, account.email as email FROM message
//                JOIN conversation_message on message.id = conversation_message.message_id
//                JOIN user_conversation on user_conversation.conversation_id = conversation_message.conversation_id
//                JOIN user on user.id = user_conversation.user_id
//                JOIN account on user.account_id = account.id';
//
//        $stmt = $conn->prepare($sql);
//        $stmt->execute();
//
//        return $stmt->fetchAll();
//    }
//
//    /**
//     * @return array
//     * @throws DBALException
//     */
//    public function getAllMessagesByMessageStatus(): array
//    {
//        $conn = $this->getEntityManager()->getConnection();
//
//        $sql = "select message.id as message_id, message.content as content, message.created_at as created_at, message.status_admin as status, message.conversation_id as conversation_id, user_conversation.user_id as user_id, user.account_id as user_account_id, account.firstname as firstname, account.lastname as lastname, account.email as email FROM message
//                JOIN conversation_message on message.id = conversation_message.message_id
//                JOIN user_conversation on user_conversation.conversation_id = conversation_message.conversation_id
//                JOIN user on user.id = user_conversation.user_id
//                JOIN account on user.account_id = account.id
//                WHERE message.status_admin = 'unread'";
//
//        $stmt = $conn->prepare($sql);
//        $stmt->execute();
//
//        return $stmt->fetchAll();
//    }
//
//    /**
//     * @param $id
//     * @return array
//     * @throws DBALException
//     */
//    public function getConversationsById($id): array
//    {
//        $conn = $this->getEntityManager()->getConnection();
//
//        $sql = 'select message.id as message_id, message.content as content, message.created_at as created_at, message.status_admin as status, message.conversation_id as conversation_id, user_conversation.user_id as user_id, user.account_id as user_account_id, account.firstname as firstname, account.lastname as lastname, account.email as email FROM message
//JOIN conversation_message on message.id = conversation_message.message_id
//JOIN user_conversation on user_conversation.conversation_id = conversation_message.conversation_id
//JOIN user on user.id = user_conversation.user_id
//JOIN account on user.account_id = account.id
//WHERE message.conversation_id = :id';
//
//        $stmt = $conn->prepare($sql);
//        $stmt->bindValue(":id", $id);
//        $stmt->execute();
//
//        return $stmt->fetchAll();
//    }
//
//    public function getConversationMessage()
//    {
//        $conn = $this->getEntityManager()->getConnection();
//        $sql = 'select conversation_id, message_id from conversation_message';
//        $stmt = $conn->prepare($sql);
//
//        $stmt->execute();
//
//        return $stmt->fetchAll();
//    }



}
